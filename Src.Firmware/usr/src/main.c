/**
  ******************************************************************************
  * @file    main.c 
  * @author  ZDA
  * @version V1
  * @date    
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2015 </center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include "main.h"
/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None 
  * @retval None
  */
  uint32_t i = 500;
void init_gpio(void)
{
	GPIO_InitTypeDef gpio_cfg;
	GPIO_StructInit(&gpio_cfg);

	/* Каналы 1 и 2 таймера TIM3 - на вход, подтянуть к питанию */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	gpio_cfg.GPIO_Mode = GPIO_Mode_IPU;
	gpio_cfg.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOA, &gpio_cfg);
	
	gpio_cfg.GPIO_Pin   = GPIO_Pin_6;
	gpio_cfg.GPIO_Mode  = GPIO_Mode_AF_PP;
	gpio_cfg.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &gpio_cfg);
	
	
	
	
	
	
	
}

void init_timer(void)
{
	TIM_TimeBaseInitTypeDef timer_base;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef        TIM_OCInitStructure;
	/* Разрешаем счёт в обе стороны, период ставим 4 */
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
  
	TIM_TimeBaseStructInit(&timer_base);
	timer_base.TIM_Period = 250;
	timer_base.TIM_CounterMode = TIM_CounterMode_Down | TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &timer_base);

	/* Считать будем все переходы лог. уровня с обоих каналов */
	TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_BothEdge, TIM_ICPolarity_BothEdge);
	
	
	// ================== Общие настройки для ШИМ =============================
	// Заполняем структуру
	TIM_TimeBaseStructure.TIM_Period             = 2000; 
	TIM_TimeBaseStructure.TIM_Prescaler          = 240;
	TIM_TimeBaseStructure.TIM_ClockDivision      = TIM_CKD_DIV4;
	TIM_TimeBaseStructure.TIM_CounterMode        = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter  = 0;

	// Заполняем конфигурацию каналов таймера
	TIM_OCInitStructure.TIM_OCMode         = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState    = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse          = 4500;
	TIM_OCInitStructure.TIM_OCPolarity     = TIM_OCPolarity_High;
	
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	// Первый канал
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	// Включаем предзагрузку
	TIM_ARRPreloadConfig(TIM3, ENABLE);
	

	TIM_Cmd(TIM2, ENABLE);
	TIM_Cmd(TIM3,  ENABLE);
}
  
  
int main(void)
{	
	Init_STM32100();
	
	init_gpio();
	init_timer();

	while (1)
	{
		TIM3->CCR1=TIM2->CNT;
	}
}



#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line) {while (1)}

#endif
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/

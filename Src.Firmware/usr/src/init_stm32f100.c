#include "init_stm32f100.h"

/*---------------- Настройка тактирования ядра ----------------------------- */ 
void RCC_Configuration(void)
{
	// Внутренний генератор HSI (8 МГц) через PLL( /2 *6 ), частота системной шины 12 МГц

	// Enable Prefetch Buffer
	FLASH_PrefetchBufferCmd( FLASH_PrefetchBuffer_Enable );
	// Цикл ожидания = 0
	FLASH_SetLatency( FLASH_Latency_0 );
	
	// Cброс настроек RCC
	RCC_DeInit();
	
	// Выключаем HSE и включаем HSI
	RCC_HSEConfig( RCC_HSE_ON );
	RCC_HSICmd( DISABLE );
	
	// Конфигурация PLL = (HSI / 2) * 6 = 12 MHz
	// Конфигурация ADC = PLL / 2 = 6 MHz
	RCC_HCLKConfig  ( RCC_SYSCLK_Div2 );
	RCC_PCLK1Config ( RCC_HCLK_Div1   );
	RCC_PCLK2Config ( RCC_HCLK_Div1   );
	RCC_ADCCLKConfig( RCC_PCLK2_Div2  );
	RCC_PLLConfig   ( RCC_PLLSource_PREDIV1, RCC_PLLMul_3 );
	RCC_SYSCLKConfig( RCC_SYSCLKSource_PLLCLK );
	RCC_PLLCmd( ENABLE );
	
	// На всякий случай отключаем системный таймер и его прерывание.
	SysTick->CTRL = 0x00000000;
}

/*-------------------------------------------------------------------------- */ 
void RTC_Configuration(void)
{
	// Включаем клоки необходимые для RTC
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE );
	// получаем доступ к Backup Domain
	PWR_BackupAccessCmd( ENABLE );
	
	// Сброс Backup Domain
	BKP_DeInit();
	
	// Уставка LSE OSC
	RCC_LSEConfig(RCC_LSE_ON);
	while ( RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET );
	// Выбор источника клоков для RTC
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	// Запуск RTC
	RCC_RTCCLKCmd( ENABLE );
	
	RTC_WaitForSynchro();
	RTC_WaitForLastTask();
//	// Разрешаем секундное прерывание
//	RTC_ITConfig(RTC_IT_SEC, ENABLE);
//	RTC_WaitForLastTask();
	// Устанавливаем период равный RTCCLK/RTC_PR = (32.768 KHz)/(32767+1)
	RTC_SetPrescaler( 32767 );
	RTC_WaitForLastTask();
}

/*-------------------------------------------------------------------------- */ 
void GPIO_Configuration(void)
{	
	GPIO_InitTypeDef GPIO_InitStruct;

	//--------- Включаем клоки ---------------------------------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA       // Порт А
	                      |RCC_APB2Periph_GPIOB       // Порт B
	                      |RCC_APB2Periph_AFIO,       // Альтернативные функции
	                       ENABLE);

	//--------- Настройка выводов для I2C1 --------------------
	//	PB6  --> I2C1_SCL
	//	PB7  --> I2C1_SDA
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);

	//--------- Настройка выводов для USART --------------------
	//	PA9  --> USART1_TX
	//	PA10 --> USART1_RX
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	//--------- Настройка SPI ---------------------------------- 
	// PB3 --> SPI_CLK
	// PB4 --> SPI_MISO
	// PB5 --> SPI_MOSI
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_3 | GPIO_Pin_5;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_4;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	// Ремап SPI
	GPIO_PinRemapConfig(GPIO_Remap_SPI1, ENABLE);
	
	//--------- Настройка выводов для ШИМ ----------------------
	//	PA0  --> SERVO-1      PA1  --> SERVO-2
	//	PA2  --> SERVO-3      PA3  --> SERVO-4
	//	PA6  --> SERVO-5      PA7  --> SERVO-6
	//	PB0  --> SERVO-7      PB1  --> SERVO-8
	//	PB14 --> SERVO-9      PB15 --> SERVO-10
	//	PA8  --> SERVO-11     PA11 --> SERVO-12
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | 
	                             GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_11;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода измерения напряжения ---- 
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_5;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AIN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	//--------- Настройка вывода звука ------------------- 
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_5;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AIN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	//--------- Настройка входа прерывания от акселирометра ----
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IPU;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка входа прерывания от Bluetooth --------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_13;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IPU;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода выбора акселирометра ----
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_15;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	//--------- Настройка вывода выбора памяти -----------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода сброса Bluetooth --------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_12;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода выключения звука --------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода светодиода --------------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_11;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	//--------- Настройка вывода включения нагрузки ------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_2;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
}

/*-------------------------------------------------------------------------- */ 
void ADC_Configuration(void)
{	
ADC_InitTypeDef ADC_InitStructure;
	// Включаем клок ADC 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); // разрешить тактирование ADC1
	// Реинициализируем модуль ADC 
	ADC_DeInit(ADC1);
	// Основные настройки
	ADC_InitStructure.ADC_Mode                = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode        = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode  = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv    = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign           = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel        = 0;
	
	ADC_Init(ADC1, &ADC_InitStructure);
	// Настраиваем инжектируемые каналы
	ADC_InjectedSequencerLengthConfig(ADC1, 1);     // 1 Канал
	ADC_InjectedChannelConfig(ADC1, ADC_Channel_0,  1, ADC_SampleTime_7Cycles5);
	// Настраиваем источник запуска инжектированных каналов - софтом
	ADC_ExternalTrigInjectedConvConfig(ADC1, ADC_ExternalTrigInjecConv_None);
	// Отключаем на всякий тригеры (Не обязательно)
	ADC_ExternalTrigInjectedConvCmd(ADC1, DISABLE);
	
	// Включаем датчик температуры
	ADC_TempSensorVrefintCmd(ENABLE);
	// Запуск модуля ADC 
	ADC_Cmd(ADC1, ENABLE);
	
	// Калибровка
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	ADC_SoftwareStartInjectedConvCmd(ADC1, ENABLE);
}

/*-------------------------------------------------------------------------- */ 
void DAC_Configuration(void)
{	
	DAC_InitTypeDef DAC_InitStruct;
	// Включаем клок DAC
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	// Заполняем структуру
	DAC_InitStruct.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
	DAC_InitStruct.DAC_OutputBuffer                 = DAC_OutputBuffer_Enable;
	DAC_InitStruct.DAC_Trigger                      = DAC_Trigger_None;
	DAC_InitStruct.DAC_WaveGeneration               = DAC_WaveGeneration_None;
	// На всякий случай сбрасываем его настройки по-умолчанию
	DAC_DeInit();
	// Инициализируем DAC_Channel_1 канал
	DAC_Init(DAC_Channel_1, &DAC_InitStruct);
	// Запускаем DAC_Channel_1
	DAC_Cmd(DAC_Channel_1, ENABLE);
}

/*-------------------------------------------------------------------------- */ 
/* TIMER configuration *****************************************************/
void TIM_Configuration(void)
{   
	/*
		Параметры для расчёта значений ШИМ
		- Частота ядра = 12 МГц
		- Период ШИМ = 60000 ед.
		- Максимальное заполнение = 10% = 6000 ед.
		- Минимальное заполнение  = 5%  = 3000 ед.
		- Средняя точка           = 15% = 4500 ед.
	
		Распределение сервоприводов по каналам:
		TIM2_CH1  --> SERVO-1     TIM3_CH3  --> SERVO-7
		TIM2_CH2  --> SERVO-2     TIM3_CH4  --> SERVO-8
		TIM2_CH3  --> SERVO-3     TIM15_CH1 --> SERVO-9
		TIM2_CH4  --> SERVO-4     TIM15_CH2 --> SERVO-10
		TIM3_CH1  --> SERVO-5     TIM1_CH1  --> SERVO-11
		TIM3_CH2  --> SERVO-6     TIM1_CH4  --> SERVO-12
	*/
	
	// Структуры 
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef        TIM_OCInitStructure;
	
	// Разрешить тактирование TIM1
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	
	// ================== Общие настройки для ШИМ =============================
	// Заполняем структуру
	TIM_TimeBaseStructure.TIM_Period             = 60000; 
	TIM_TimeBaseStructure.TIM_Prescaler          = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision      = TIM_CKD_DIV4;
	TIM_TimeBaseStructure.TIM_CounterMode        = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter  = 0;

	// Заполняем конфигурацию каналов таймера
	TIM_OCInitStructure.TIM_OCMode         = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState    = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse          = 4500;
	TIM_OCInitStructure.TIM_OCPolarity     = TIM_OCPolarity_High;
	
	// ================== Таймер TIM1 =========================================
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	// Первый канал
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	// Четвертый канал
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);
	// Включаем предзагрузку
	TIM_ARRPreloadConfig(TIM1, ENABLE);
	// Разрешаем ШИМ от таймера TIM1
	// Эта штука обязательна для этого таймера
	TIM_CtrlPWMOutputs(TIM1,ENABLE);
	
	// ================== Таймер TIM2 =========================================
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	// Первый канал
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);
	// Второй канал
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
	// Третий канал
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
	// Четвертый канал
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);
	// Включаем предзагрузку
	TIM_ARRPreloadConfig(TIM2, ENABLE);
	
	// ================== Таймер TIM3 =========================================
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	// Первый канал
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	// Второй канал
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	// Третий канал
	TIM_OC3Init(TIM3, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
	// Четвертый канал
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
	// Включаем предзагрузку
	TIM_ARRPreloadConfig(TIM3, ENABLE);
	
	// ================== Таймер TIM15 ========================================
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM15, &TIM_TimeBaseStructure);
	// Первый канал
	TIM_OC1Init(TIM15, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM15, TIM_OCPreload_Enable);
	// Четвертый канал
	TIM_OC2Init(TIM15, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM15, TIM_OCPreload_Enable);
	// Включаем предзагрузку
	TIM_ARRPreloadConfig(TIM15, ENABLE);
	
	// ================== Таймеры =============================================
	// Запуск таймеров
	TIM_Cmd(TIM1,  ENABLE);
	TIM_Cmd(TIM2,  ENABLE);
	TIM_Cmd(TIM3,  ENABLE);
	TIM_Cmd(TIM15, ENABLE);
}

/*-------------------------------------------------------------------------- */ 
void I2C_Configuration(void)
{	
	I2C_InitTypeDef I2C_InitStructure;

	// Включаем клок для I2C
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	
	// Сбрасываем настроки на всякий случай
	I2C_DeInit(I2C1);
	// Заполняем структуру с настройками
	I2C_InitStructure.I2C_Ack                  = I2C_Ack_Disable;
	I2C_InitStructure.I2C_AcknowledgedAddress  = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed           = 4000;
	I2C_InitStructure.I2C_DutyCycle            = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Mode                 = I2C_Mode_I2C;
	I2C_InitStructure.I2C_OwnAddress1          = 0x00;
	
	// Инициализация I2C1
	I2C_Init(I2C1, &I2C_InitStructure);
	// Включаем I2C1
	I2C_Cmd(I2C1, ENABLE);
}

/*-------------------------------------------------------------------------- */ 
void SPI_Configuration(void)
{	
	SPI_InitTypeDef SPI_InitStructure;
	
	// Включаем клок для SPI
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	
	// Сбрасываем настроки на всякий случай
	SPI_I2S_DeInit(SPI1);
	// Заполняем структуру с настройками
	SPI_InitStructure.SPI_Direction         = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode              = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize          = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL              = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA              = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS               = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
	SPI_InitStructure.SPI_FirstBit          = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial     = 0;
	
	// Инициализация
	SPI_Init(SPI1, &SPI_InitStructure);
	// Включаем
	SPI_Cmd(SPI1, ENABLE);
}

/*-------------------------------------------------------------------------- */
void Init_STM32100(void)
{
	RCC_Configuration();            // Настройка тактирования ядра
	//RTC_Configuration();            // Настройка часов реального времени
	//GPIO_Configuration();           // Настройка портов ввода/вывода
	//ADC_Configuration();            // Настройка аналого-цифрового преобразователя
	//DAC_Configuration();            // Настройка аналого-цифрового преобразователя
	//SPI_Configuration();            // Настройка модуля SPI
	//TIM_Configuration();            // Настройка таймеров
	//I2C_Configuration();            // Настройка модуля I2C
}

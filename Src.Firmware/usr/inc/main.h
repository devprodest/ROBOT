/***********************************************************************************************************/
/*            Основные константы и переменные                                                              */
/***********************************************************************************************************/
#ifndef		MAIN_H
#define		MAIN_H 

//----- Инклуды ---------------------------------------------------------------------------------------------
#include <stdint.h>
#include "stm32f10x_conf.h"
#include "stm32f10x.h"
#include "stm32f10x_it.h"
#include "init_stm32f100.h"
#include "usr_math.h"
#include "servo.h"

//----- Дефайны ---------------------------------------------------------------------------------------------

//----- Переменные и структуры ------------------------------------------------------------------------------

//----- КОНЕЦ ФАЙЛА -----------------------------------------------------------------------------------------
#endif
